# frozen_string_literal: true

#
# Service object to create Groups.
class GroupCreator < DrawlessGroupCreator
  private

  def process_params
    super
    add_draw_to_params
  end

  def add_draw_to_params
    return unless params[:leader_id].present?
    @params.merge!(draw_id: User.find(params[:leader_id]).draw.id)
  end

  def ensure_valid_members; end

  def success
    {
      redirect_object: [group.draw, group], group: group,
      msg: { success: "#{group.name} created." }
    }
  end

  def validate_suite_size_inclusion
    draw = Draw.find(User.find(params[:leader_id]).draw.id)
    return if draw.open_suite_sizes.include? params[:size].to_i
    errors.add :size, 'must be an available suite size in the draw'
  end
end
